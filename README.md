# Welcome to GitLab Gateway!

This is a curated collection of "good first issues" from GitLab, aimed at ushering in newcomers to the open-source community. Built with Hugo and love, this site makes contributing to GitLab projects more accessible and less intimidating.

## Features

- **Find Your Fit:** Filter through issues labeled as 'good first issue' from various GitLab repositories.
- **First-Timer Friendly:** Step-by-step guides for making your first open-source contribution.
- **Community-Centric:** Connect with other contributors through a built-in forum.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What you need to install the software and how to install them:
```
git clone git@gitlab.com:familieslifedev/good-first-issues.git
cd good-first-issues
```

### Installing

A step-by-step series of examples that tell you how to get a development environment running:

```bash
# Install Hugo (You'll need to check Hugo's official docs for the method best suited for your OS)
brew install hugo
# OR
port install hugo
# OR
choco install hugo -confirm

# Serve the site locally
hugo server
```
Now, head over to http://localhost:1313 in your browser, and you should see the site running.

# Deployment

To deploy this on a live system, push your changes to the GitLab repo, and GitLab Pages will automatically deploy your site:
```
git add .
git commit -m "Commit message"
git push -u origin master
```

# Built With

- Hugo [https://gohugo.io/] - The world’s fastest framework for building websites.
- GitLab Pages - Hosting service provided by GitLab.

# Contributing

Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting merge requests to me.

# Author

Charlotte - Initial work

See also the list of contributors who participated in this project.

# License

This project is licensed under the MIT License - see the LICENSE.md file for details.

# Acknowledgments

# Disclaimer

This site is not affiliated with, endorsed by, or directly connected to GitLab Inc. This is a community-driven project designed to simplify the journey for new open-source enthusiasts.