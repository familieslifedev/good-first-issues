document.addEventListener('DOMContentLoaded', function() {
    const buttons = document.querySelectorAll('.filter-button');
    const issues = document.querySelectorAll('.issue');

    buttons.forEach(button => {
        button.addEventListener('click', function() {
            const filterType = this.hasAttribute('data-language') ? 'language' : 'difficulty';
            const filterValue = this.dataset[filterType];

            issues.forEach(issue => {
                const issueLanguages = issue.dataset.languages ? issue.dataset.languages.split(',') : [];
                const issueDifficulty = issue.dataset.difficulty;

                const displayIssue = (filterType === 'language' && issueLanguages.includes(filterValue)) ||
                    (filterType === 'difficulty' && issueDifficulty === filterValue) ||
                    filterValue === 'all';

                issue.style.display = displayIssue ? 'block' : 'none';
            });
        });
    });
});
