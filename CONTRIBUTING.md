# Contributing to GitLab Gateway

First off, thanks for taking the time to contribute! 🌟

The following is a set of guidelines for contributing to GitLab Gateway, which is hosted on GitLab at https://gitlab.com/familieslifedev/good-first-issues. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a merge request.

## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report for GitLab Gateway. Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

**Before Submitting A Bug Report**

- Check the issues on our website for a list of current known issues.
- Perform a cursory search to see if the bug has already been reported. If it has, add a comment to the existing issue instead of opening a new one.

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for GitLab Gateway, including completely new features and minor improvements to existing functionality.

### Your First Code Contribution

Unsure where to begin contributing to GitLab Gateway? You can start by looking through these `beginner` and `help-wanted` issues:

- Beginner issues - issues which should only require a few lines of code, and a test or two.
- Help wanted issues - issues which should be a bit more involved than `beginner` issues.

## Merge Requests

The process described here has several goals:

- Maintain GitLab Gateway's quality
- Fix problems that are important to users
- Engage the community in working toward the best possible GitLab Gateway
- Enable a sustainable system for GitLab Gateway's maintainers to review contributions

Please follow these steps for your contribution:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. Ensure the test suite passes.
4. Make sure your code lints.
5. Issue that merge request!

## Styleguides

### Git Commit Messages

- Use the present tense ("Add feature" not "Added feature")
- Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
- Limit the first line to 72 characters or less
- Reference issues and pull requests liberally after the first line

### JavaScript Styleguide

All JavaScript must adhere to [JavaScript Standard Style](https://standardjs.com/).

## Code of Conduct

Contributors are expected to uphold the [Code of Conduct](CODE_OF_CONDUCT.md).
