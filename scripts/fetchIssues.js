/**
 * @typedef {Object} GitLabIssue
 * @property {number} iid
 * @property {string} created_at
 * @property {string} web_url
 * @property {string} title
 * @property {string} description
 */

const axios = require('axios');
const fs = require('fs');
const path = require('path');

const GITLAB_TOKEN = process.env.GITLAB_TOKEN;
const PROJECT_ID = process.env.PROJECT_ID;
const ISSUES_DIRECTORY = path.join(__dirname, '../content/issues');
const ISSUES_INDEX_PATH = path.join(__dirname, '../content/issues.md');

const fetchIssues = async () => {
    const apiUrl = `https://gitlab.com/api/v4/projects/${encodeURIComponent(PROJECT_ID)}/issues?scope=all&state=opened&labels=quick%20win`;
    console.log(`Requesting URL: ${apiUrl}`);

    const config = {
        headers: {'PRIVATE-TOKEN': GITLAB_TOKEN}
    };

    try {
        const response = await axios.get(apiUrl, config);

        if (!fs.existsSync(ISSUES_DIRECTORY)) {
            fs.mkdirSync(ISSUES_DIRECTORY, { recursive: true });
        }

        let issuesIndexContent = `---\ntitle: \"Issues\"\nfeatured_image: \'\'\nomit_header_text: true\ndescription: First issues for newcomers\ntype: page\nmenu: main\n---
        \n\nWelcome to the Issues page! Here, you can find a curated list of open issues perfect for newcomers to the open-source community. Dive in and find where you can contribute.\n
        \n{{< disclaimer >}}\n
        \n## Open Issues\n\nHere\'s a list of open issues:\n\n`

        response.data.forEach(issue => {
            const issueFileName = `${issue.iid}.md`;
            const issueFilePath = path.join(ISSUES_DIRECTORY, issueFileName);
            const issueContent = `---
summary: "${issue.title.replace(/"/g, '\\"')}"
date: ${issue.created_at}
issue_link: "${issue.web_url}"
---

${issue.description}
`;

            try {
                fs.writeFileSync(issueFilePath, issueContent);
                console.log(`Issue file written for issue ${issue.iid}`);
            } catch (writeError) {
                console.error(`Error writing file for issue ${issue.iid}:`, writeError);
                return; // Skip this issue if the file can't be written
            }

            // Construct the link for the issue to be inserted into the main issues page
            const issueLink = `- [${issue.title}](../issues/${issue.iid}.md) - ${new Date(issue.created_at).toLocaleDateString()}\n`;

            // Prevent duplicate entries
            if (!issuesIndexContent.includes(issueLink)) {
                issuesIndexContent += issueLink;
            }
        });

        // Ensure the issues index content ends with a newline
        issuesIndexContent += '\n';

        // Write the updated content back to the main issues page once after all issues are processed
        fs.writeFileSync(ISSUES_INDEX_PATH, issuesIndexContent);
        console.log('Main issues page updated with issue links.');

    } catch (error) {
        console.error('Error fetching issues:', error);
    }
};

fetchIssues().then(() => {
    console.log('Issues have been fetched successfully.');
}).catch(error => {
    console.error('An error occurred while fetching issues:', error);
});
