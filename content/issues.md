---
title: "Issues"
featured_image: ''
omit_header_text: true
description: First issues for newcomers
type: page
menu: main
---
        

Welcome to the Issues page! Here, you can find a curated list of open issues perfect for newcomers to the open-source community. Dive in and find where you can contribute.

        
{{< disclaimer >}}

        
## Open Issues

Here's a list of open issues:

- [Update issuable label docs (created if doesn't exists)](../issues/454576.md) - 4/4/2024
- [Follow-up from "Refactor LfsStorageController#upload_finalize into service"](../issues/454436.md) - 4/4/2024
- [Add help text for agent selector on environment settings page](../issues/454329.md) - 4/3/2024
- ["Remove group" text ambiguously suggests upcoming deletion](../issues/450805.md) - 3/19/2024
- [Add a `aimed_for_deletion` and `include_hidden` argument to `Query.projects`](../issues/450190.md) - 3/12/2024
- [Follow-up: Reorder token options to be sorted alphabetically](../issues/448885.md) - 3/8/2024
- [Extract getPayload() method to helper/util library (from crm_form and achievements_form)](../issues/446173.md) - 3/6/2024
- [Do not allow creating Service Accounts for a subgroup](../issues/446140.md) - 3/6/2024
- [Failures in spec/models/preloaders/user_max_access_level_in_projects_preloader_spec.rb:38 and 45 and 56](../issues/446114.md) - 3/6/2024
- [Provide AWS CodeDeploy with the package version number in deploy_to_ec2](../issues/444750.md) - 3/5/2024
- [Failures in spec/models/preloaders/user_max_access_level_in_projects_preloader_spec.rb:22](../issues/444712.md) - 3/5/2024
- [Move call to `AiRelatedSettingsChangedEvent`](../issues/444224.md) - 3/1/2024
- [Accessing repository with incorrect basic authentication header results in error `500`](../issues/443796.md) - 2/29/2024
- [Limit maximum `vulnerabilities_allowed` to not overflow `smallint`](../issues/443290.md) - 2/26/2024
- [Output a log message when Gemnasium skips a dependency](../issues/442493.md) - 2/21/2024
- [UX bug: overridden info should be below Summary part](../issues/442399.md) - 2/20/2024
- [Audit Event when Admin stops impersonating](../issues/442214.md) - 2/16/2024
- [Move Pipeline#security_reports to EE code base](../issues/442035.md) - 2/15/2024
- [Include prereleases if affected range starts with a final release](../issues/442028.md) - 2/15/2024
- [Follow-up from "Rename CreateEmptyEmbeddingsRecordsWorker"](../issues/441766.md) - 2/14/2024

