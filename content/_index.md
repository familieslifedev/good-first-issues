---
title: "GitLab Gateway"

description: "A Hugo site built with GitLab Pages to curate beginner-friendly issues to welcome newcomers and realise GitLab's mission."
cascade:
  featured_image: '/images/hero.jpg'
---
