---
summary: "Failures in spec/models/preloaders/user_max_access_level_in_projects_preloader_spec.rb:22"
date: 2024-03-05T16:59:24.163Z
issue_link: "https://gitlab.com/gitlab-org/gitlab/-/issues/444712"
---

This test `spec/models/preloaders/user_max_access_level_in_projects_preloader_spec.rb:22` can only pass with `rspec-retry`, failing with: https://gitlab.com/gitlab-org/gitlab/-/jobs/6321495454

```
Failures:
  1) Preloaders::UserMaxAccessLevelInProjectsPreloader without preloader runs some queries
     Failure/Error: expect { query }.to make_queries(projects.size + 3)
       expected to make 8 queries but made 9 queries
     # ./spec/models/preloaders/user_max_access_level_in_projects_preloader_spec.rb:26:in `block (3 levels) in <main>'
```

It's quarantined in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145653
