---
summary: "Limit maximum `vulnerabilities_allowed` to not overflow `smallint`"
date: 2024-02-26T08:06:31.054Z
issue_link: "https://gitlab.com/gitlab-org/gitlab/-/issues/443290"
---

<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "type::bug" label:

- https://gitlab.com/gitlab-org/gitlab/issues?label_name%5B%5D=regression
- https://gitlab.com/gitlab-org/gitlab/issues?label_name%5B%5D=type::bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

When a policy defines `vulnerabilities_allowed` larger than a value of `smallint`, we error on policy sync with `out of range for ActiveModel::Type::Integer with limit 2 bytes`.

We should limit this value to a maximum of `32767` - the range of `smallint` is from -32768 to +32767.

### Steps to reproduce

Create a policy with the following YAML:

```yaml
type: approval_policy
name: Test
description: ''
enabled: true
rules:
  - type: scan_finding
    scanners: []
    vulnerabilities_allowed: 100000
    severity_levels: []
    vulnerability_states: []
    branch_type: protected
actions:
  - type: require_approval
    approvals_required: 1
    role_approvers:
      - developer
approval_settings:
  block_branch_modification: true
  prevent_pushing_and_force_pushing: true
```

### Example Project

<!-- If possible, please create an example project here on GitLab.com that exhibits the problematic 
behavior, and link to it here in the bug report. If you are using an older version of GitLab, this 
will also determine whether the bug is fixed in a more recent version. -->

### What is the current *bug* behavior?

We have errors when syncing policies and the approval rules are not applied.

[Kibana](https://log.gprd.gitlab.net/app/r/s/xEakJ)

### What is the expected *correct* behavior?

We shouldn't allow such policy to be created.

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->

### Output of checks

<!-- If you are reporting a bug on GitLab.com, uncomment below -->

<!-- This bug happens on GitLab.com -->
<!-- /label ~"reproduced on GitLab.com" -->

#### Results of GitLab environment info

<!--  Input any relevant GitLab environment information if needed. -->

<details>
<summary>Expand for output related to GitLab environment info</summary>

<pre>

(For installations with omnibus-gitlab package run and paste the output of:
`sudo gitlab-rake gitlab:env:info`)

(For installations from source run and paste the output of:
`sudo -u git -H bundle exec rake gitlab:env:info RAILS_ENV=production`)

</pre>
</details>

#### Results of GitLab application Check

<!--  Input any relevant GitLab application check information if needed. -->

<details>
<summary>Expand for output related to the GitLab application check</summary>
<pre>

(For installations with omnibus-gitlab package run and paste the output of:
`sudo gitlab-rake gitlab:check SANITIZE=true`)

(For installations from source run and paste the output of:
`sudo -u git -H bundle exec rake gitlab:check RAILS_ENV=production SANITIZE=true`)

(we will only investigate if the tests are passing)

</pre>
</details>

### Possible fixes

There are 2 ways to prevent this:

- Validate `maximum` using the json schema. This is easier but existing policies would disappear from the list and must be fixed directly in `policy.yml` in the Security Policy Project repository.
    
```diff
diff --git a/ee/app/validators/json_schemas/security_orchestration_policy.json b/ee/app/validators/json_schemas/security_orchestration_policy.json
index 2c20ce3d32fa..5880c18e40f8 100644
--- a/ee/app/validators/json_schemas/security_orchestration_policy.json
+++ b/ee/app/validators/json_schemas/security_orchestration_policy.json
@@ -568,7 +568,8 @@
                 "vulnerabilities_allowed": {
                   "description": "Specifies a number of vulnerabilities allowed before this rule is enforced.",
                   "type": "integer",
-                  "minimum": 0
+                  "minimum": 0,
+                  "maximum": 32767
                 },
                 "severity_levels": {
                   "description": "Specifies a list of vulnerability security levels that should be concidered to enforce this policy. Possible values: `info`, `unknown`, `low`, `medium`, `high`, `critical`.",
```

- Validate in code using [`ValidatePolicyService`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/services/security/security_orchestration_policies/validate_policy_service.rb)
