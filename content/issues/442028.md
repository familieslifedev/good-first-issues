---
summary: "Include prereleases if affected range starts with a final release"
date: 2024-02-15T11:32:32.110Z
issue_link: "https://gitlab.com/gitlab-org/gitlab/-/issues/442028"
---

# Problem to solve

When the affected range of versions of a security advisory starts with a final release like `1.0.0`,
it's likely that prereleases like `1.0.0-rc1` are also affected.

# Proposal

Err on the side of caution and mark a prerelease as affected when the affected range of versions starts with a final release (that matches the prerelease). For instance, mark a project dependency as affected if it depends on `1.0.0-rc1` of a package, and the affected range is `>=1.0.0` (and all other criteria match).
