---
summary: "Update issuable label docs (created if doesn't exists)"
date: 2024-04-04T17:01:01.552Z
issue_link: "https://gitlab.com/gitlab-org/gitlab/-/issues/454576"
---

In https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148449+ the docs on the REST API for create an issue were updated to note that if a label name is provided upon creation, the label will be created if it doesn't exist and then assigned to the new issue. There are other places where this is also true as noted in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148449#note_1844422138

1. Issue update API https://gitlab.com/gitlab-org/gitlab/-/blob/ac053b7af9b3ce34511eb2a31e2c07b449b49c03/doc/api/issues.md?blame=1#L1209
2. For update and create issues, the `add_labels` (undocumented in create action, maybe because `add` doesn't make a lot of sense when creating a new issue, but it is supported) argument will do the same https://gitlab.com/gitlab-org/gitlab/-/blob/ac053b7af9b3ce34511eb2a31e2c07b449b49c03/doc/api/issues.md?blame=1#L1200

This is also true for other issuables like MRs, https://gitlab.com/gitlab-org/gitlab/-/blob/ac053b7af9b3ce34511eb2a31e2c07b449b49c03/doc/api/merge_requests.md?plain=1#L1310
