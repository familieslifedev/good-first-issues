---
summary: "Audit Event when Admin stops impersonating"
date: 2024-02-16T16:09:59.287Z
issue_link: "https://gitlab.com/gitlab-org/gitlab/-/issues/442214"
---

\~\~### Problems to solve:

~~The problem is related to the auditors, which are checking how the GitLab platform complies with regulations in the financial industry. Therefore an important piece of detail, which our customer wants to see implemented to be able to comply with regulations.~~

* ~~the impersonation event of entering/leaving this mode are recorded, but the actions are not recorded and therefore no one knows what happened.~~
* ~~the impersonation allows to do it on multiple levels, meaning, you can impersonate to another admin user, than impersonate again etc. which is making it even harder and hiding actually then in the trail of events what happened (see above)~~
* ~~in another software solution, a similar impersonation feature, but the user has to provide a "root cause" (popup window with free text) why he is impersonating, and only if user provided a root cause / reason, he/she is able to proceed with the impersonation. this text is then also logged and enables them to have much better insights, about which user did something and why, when auditors are coming and check their platform.~~

### Update

:point_up: Most of this is implemented, see thread starting [here](https://gitlab.com/gitlab-org/gitlab/-/issues/442214#note_1790392030 "Audit Event when Admin stops impersonating")

### Related

* Slack conversation (internal: https://gitlab.slack.com/archives/C0610LVCSAY/p1708083880870919)
* Workflow improvements
* Audit/Logging of events
* Time expiration of impersonation\~\~

### New Requirements

Add the following audit events:

1. If impersonating a user and trigger the log out action, create a `user_impersonation` audit event
2. If impersonating a user and close tab, create a `user_impersonation` audit event
3. If impersonating a user and session expires, create a `user_impersonation` audit event

https://gitlab.com/gitlab-org/gitlab/-/issues/442214#note_1794347513
